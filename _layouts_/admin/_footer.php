
</div>
</div>
</div>
<!-- End of Page Wrapper -->

<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
</a>

<!-- Logout Modal-->
<div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">¿Confirma el cierre de sesión?</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">Click en "Salir" si desea salir del Portal de Global PRL. </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancelar</button>
                <a class="btn btn-danger" href="/welcome/login/logout">Salir</a>
            </div>
        </div>
    </div>

</div>

<!-- MODAL TINYmce -->
<div class="modal fade" id="modalTiny"  tabindex="-1"
     role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header bg-gray-400">
                <h5 class="modal-title text-primary"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-remote"> <div class="overlay" id="overlay_modal" align="center">
                    <div class="text-mutted">
                        <i class="fa fa-refresh fa-spinner fa-4x"></i>
                        <p>
                            Loading...
                        </p>
                    </div>
                </div></div>
        </div>
    </div>
</div>
<div class="modal fade" id="modalSec"  tabindex="-1"
     role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header bg-dark">
                <h5 class="modal-title text-light"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-remote"> <div class="overlay" id="overlay_modal" align="center">
                    <div class="text-mutted">
                        <i class="fa fa-refresh fa-spinner fa-4x"></i>
                        <p>
                            Loading...
                        </p>
                    </div>
                </div></div>
        </div>
    </div>
</div>
<!-- MODAL TINYmce -->
<div class="modal fade" id="modalTerc"  tabindex="-1"
     role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header bg-info">
                <h5 class="modal-title text-primary"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-remote"> <div class="overlay" id="overlay_modal" align="center">
                    <div class="text-mutted">
                        <i class="fa fa-refresh fa-spinner fa-4x"></i>
                        <p>
                            Loading...
                        </p>
                    </div>
                </div></div>
        </div>
    </div>
</div>

<!-- modalLg -->
<div class="modal fade" id="modalLg"  tabindex="-1"
     role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header bg-gray-800">
                <h5 class="modal-title text-white"></h5>
                <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-remote"> <div class="overlay" id="overlay_modal" align="center">
                    <div class="text-mutted">
                        <i class="fa fa-refresh fa-spinner fa-4x"></i>
                        <p>
                            Loading...
                        </p>
                    </div>
                </div></div>
        </div>
    </div>
</div>
<!-- modalWarning -->
<div class="modal fade" id="modalWarning"  tabindex="-1"
     role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header bg-gradient-warning">
                <h5 class="modal-title text-white"></h5>
                <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-remote"> <div class="overlay" id="overlay_modal" align="center">
                    <div class="text-mutted">
                        <i class="fa fa-refresh fa-spinner fa-4x"></i>
                        <p>
                            Loading...
                        </p>
                    </div>
                </div></div>
        </div>
    </div>
</div>
<!-- modalInfo -->
<div class="modal fade" id="modalInfo"  tabindex="-1"
     role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header bg-gradient-info">
                <h5 class="modal-title text-white"></h5>
                <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-remote"> <div class="overlay" id="overlay_modal" align="center">
                    <div class="text-mutted">
                        <i class="fa fa-refresh fa-spinner fa-4x"></i>
                        <p>
                            Loading...
                        </p>
                    </div>
                </div></div>
        </div>
    </div>
</div>
<!-- modaPrimary -->
<div class="modal fade" id="modalPrimary"  tabindex="-1"
     role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header bg-gradient-primary">
                <h5 class="modal-title text-white"></h5>
                <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-remote"> <div class="overlay" id="overlay_modal" align="center">
                    <div class="text-mutted">
                        <i class="fa fa-refresh fa-spinner fa-4x"></i>
                        <p>
                            Loading...
                        </p>
                    </div>
                </div></div>
        </div>
    </div>
</div>
<div class="modal fade" id="modalSelect"  tabindex="-1"
     role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header bg-dark">
                <h5 class="modal-title text-white"></h5>
                <button type="button" class="close text-white" onclick="location.reload()">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-remote"> <div class="overlay" id="overlay_modal" align="center">
                    <div class="text-mutted">
                        <i class="fa fa-refresh fa-spinner fa-4x"></i>
                        <p>
                            Loading...
                        </p>
                    </div>
                </div></div>
        </div>
    </div>
</div>
<!-- modalLg -->
<div class="modal fade" id="modalResult"  tabindex="-1"
     role="dialog" aria-labelledby="myResultModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <div class="modal-body" id="result-search"> 
            </div>


        </div>
    </div>
</div>
<!-- modalLg end -->

<!-- modaltiny end -->


<!-- Bootstrap core JavaScript-->

<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/js/bootstrap.bundle.min.js"></script>

<!-- Core plugin JavaScript-->
<script src="/_styles/sbadmin2/vendor/jquery-easing/jquery.easing.min.js"></script>

<!-- Custom scripts for all pages-->
<script src="/_styles/sbadmin2/js/sb-admin-2.min.js"></script>

<script src="/src/js/utils.js"></script>

<!-- Page level plugins -->

<?php foreach ($plugins AS $plugin): ?>
    <script src="/src/js/<?php echo $plugin; ?>"></script>

<?php endforeach; ?>
<script>

//                    window.setInterval(function () {
//                        loadUnReadMessages();
//                    }, 30000);
//

                    $(".clickable-row").click(function () {
                        window.location = $(this).data("href");

                    });


                    // fix for tinymce in modal divs 
                    $('#modalTiny').on('hide.bs.modal', function () {
                        // scope the selector to the modal so you remove any editor on the page underneath.
                        tinymce.remove('#modalTiny textarea');
                    });


                    $(document).on('keyup keypress', 'form input[type="text"]', function (e) {
                        if (e.keyCode === 13) {
                            e.preventDefault();
                            return false;
                        }
                    });


                    /**
                     * Load results from top searcher
                     * @param {type} string
                     * @returns {undefined}
                     */
                    function searchString(string) {

                        if (string !== '') {
                            to_search = string.trim().replace(/ /g, '%20');

                            $('#result-search').load('/admin/dashboard/search/' + to_search, function () {
                                $('#modalResult').modal({show: true});
                            });
                        }


                    }

                    // open remote data in windows modal bootstrap4, dprecated data-remote by default
                    $('body').on('click', '[data-toggle="modal"]', function () {
                        $($(this).data("target") + ' .modal-remote').load($(this).data("remote"));
                    });
                    $('.modal').on('show.bs.modal', function (event) {
                        var button = $(event.relatedTarget); // Button that triggered the modal
                        var recipient = button.data('titlemodal'); // Extract info from data-* attributes
                        // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
                        // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
                        var modal = $(this);
                        modal.find('.modal-title').text(recipient);
                    });

</script>

</body>

</html>
