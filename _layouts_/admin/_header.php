<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title><?php echo __APPNAME__; ?></title>
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Latest compiled and minified CSS -->
        <!-- Custom styles for this template-->
         <link href="/_styles/sbadmin2/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
        <!-- Custom styles for this template-->
        <link href="/_styles/sbadmin2/css/sb-admin-2.min.css" rel="stylesheet">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

    </head>
    <body id="page-top">
        <div id="wrapper">
            <?php
            
            if (isset($_SESSION['Q_id_user'], $_SESSION['Q_username'], $_SESSION['Q_login_string']) AND $display) {
                include '_sidebar.php';
                include '_top.php';
            }
            ?>
            <div id="response_proc"></div>
