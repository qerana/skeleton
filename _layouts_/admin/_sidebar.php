<!-- SIDEBAR INIT -->
<ul class="navbar-nav bg-gradient-light sidebar sidebar-dark accordion toggled" id="accordionSidebar">

    <!-- Sidebar - Brand -->
    <a class=" d-flex align-items-center" href="<?php echo __URL__ . '/admin/dashboard/index'; ?>">
        <div class="sidebar-brand-icon text-warning">
            <img src="/src/img/logo_global.png" class="img-fluid">
        </div>
    </a>

    <!-- Divider -->
    <hr class="sidebar-divider my-0">


    <!-- Divider -->
    <hr class="sidebar-divider">

    <?php foreach ($_SESSION['navmap_modules'] AS $Module):
        echo $Module->nav_content;
    endforeach; ?>



    <!-- Divider -->
    <hr class="sidebar-divider d-none d-md-block">

    <!-- Sidebar Toggler (Sidebar) -->
    <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0 bg-gray-400" id="sidebarToggle"></button>
    </div>

</ul>
<!-- END SIDEBAR-->
