<!-- INIT TOPBAR -->
<!-- Content Wrapper -->
<div id="content-wrapper" class="d-flex flex-column">

    <!-- Main Content -->
    <div id="content">
        <nav class="navbar navbar-expand navbar-light topbar mb-4 static-top shadow" style="background-color: ghostwhite;">

            <!-- Sidebar Toggle (Topbar) -->
            <button id="sidebarToggleTop" class="btn btn-danger d-md-none rounded-circle mr-3">
                <i class="fa fa-bars"></i>
            </button>

            <!-- Topbar Search -->
            <div id="topFormSearch" 
                 class="d-none d-sm-inline-block form-inline mr-auto ml-md-3 my-2 my-md-0 mw-100 navbar-search">
                <div class="input-group">
                    <input type="text" class="form-control bg-light border-1 small" name="string_search" id="string_search"
                           placeholder="Buscar..." aria-label="Search" aria-describedby="basic-addon2"
                           onchange="searchString(this.value)" >
                    <div class="input-group-append">
                        <button class="btn btn-danger" type="button" 
                                onclick="searchString($('#string_search').val())">
                            <i class="fas fa-search fa-sm"></i>
                        </button>
                    </div>
                </div>
            </div>

            <!-- Topbar Navbar -->
            <ul class="navbar-nav ml-auto">

                <!-- Nav Item - Search Dropdown (Visible Only XS) -->
                <li class="nav-item dropdown no-arrow d-sm-none">
                    <a class="nav-link dropdown-toggle" href="#" id="searchDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fas fa-search fa-fw"></i>
                    </a>
                    <!-- Dropdown - Messages -->
                    <div class="dropdown-menu dropdown-menu-right p-3 shadow animated--grow-in" aria-labelledby="searchDropdown">
                        <div class="input-group">
                            <input type="text" class="form-control bg-light border-0 small" 
                                   name="string_search_m" id="string_search_m"
                                   placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2">
                            <div class="input-group-append"
                                 onchange="searchString(this.value)">
                                <button class="btn btn-danger" type="button">
                                    <i class="fas fa-search fa-sm" 
                                       onclick="searchString($('#string_search_m').val())"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </li>
                <li class="nav-item dropdown no-arrow mx-1">
                    <a class="nav-link dropdown-toggle" href="#" id="messagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fas fa-envelope fa-fw"></i>
                        <!-- Counter - Messages -->
                        <span class="badge badge-danger badge-counter" id="num_message"></span>
                    </a>
                    <!-- Dropdown - Messages -->
                    <div class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="messagesDropdown">
                        <h6 class="dropdown-header">
                            <a href="/muro/message/myMessages" class="text-white">Mis mensajes recibidos</a>
                        </h6>
                        <div id="unread_messages" >
                            <a class="dropdown-item text-center small text-gray-500"
                               href="/muro/message/myMessages">Ver todos</a>
                        </div>
                </li>

                <div class="topbar-divider d-none d-sm-block"></div>

                <!-- Nav Item - User Information -->
                <li class="nav-item dropdown no-arrow">
                    <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <span class="mr-2 d-none d-lg-inline text-warning "><?php echo $_SESSION['Q_username']; ?></span>
                        <span class="text-warning"><i class="fa fa-user"></i></span>
                    </a>
                    <!-- Dropdown - User Information -->
                    <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                        <a class="dropdown-item" href="/welcome/welcome/profile">
                            <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                            Perfil
                        </a>
                        <a class="dropdown-item" href="/qpanel/setting/index" target="_blank">
                            <i class="fas fa-cogs fa-sm fa-fw mr-2 text-gray-800"></i>

                            Panel de control
                        </a>

                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="" data-toggle="modal" data-target="#logoutModal">
                            <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-danger" title="Salir de la app"></i>
                            Hasta luego!!
                        </a>
                    </div>
                </li>

            </ul>

        </nav>
        <!-- ENT TOPBAR -->
        <div id='qfw_content'>

        </div>

        <script>
//            window.setInterval(function () {
//                loadUnReadMessages();
//            }, 30000);

        </script>
