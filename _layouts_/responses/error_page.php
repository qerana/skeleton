<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-15" />
         <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="lang" content="es" />
        <meta name="author" content="qerana" />
        <meta name="organization" content="qerana" />
        <meta name="locality" content="qworld" />
        <title>RuntimeError!</title>
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
            <link href="/_styles/sbadmin2/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
        <link href="/_styles/sbadmin2/css/sb-admin-2.min.css" rel="stylesheet">
    </head>
    <body style="background-color: #F8F8F8;" >
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-xl-6 col-lg-12 col-md-9">
                    <div class="card mb-4 shadow-lg">
                        <div class="card-header" style="background-color: LightCoral">
                            <div class="text-center font-weight-bold">
                                <h3 class="h2 text-white mb-2"> 
                                    <i class="fa fa-sad-tear fa-2x"></i><br>
                                    <?php echo $title; ?></h3>

                            </div>
                        </div>
                        <div class="card-body" style="background-color: LavenderBlush">
                            <p class="text-danger mt-0 pt-0"><?php echo $description; ?></p>
                        </div>

                        <div class="card-footer">
                            <?php if ($sw_goback == 'goback') { ?>
                                <button class="btn btn-secondary btn-block"  onclick='history.back()'>Volver</button>
                            <?php } else if($sw_goback == 'close') {?>
                                <button class="btn btn-light bg-gray-300 btn-block"  onclick='$("#response_proc").addClass("d-none")'>Cerrar</button>
                            <?php } ?>
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </body>
</html>
<?php
die();
