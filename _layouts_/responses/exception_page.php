<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-15" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="lang" content="es" />
        <meta name="author" content="qerana" />
        <meta name="organization" content="qerana" />
        <meta name="locality" content="qworld" />
        <title>Error500</title>
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    </head>
    <body style="background-color: #778899;" >
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-xl-6 col-lg-12 col-md-9">
                    <div class="card mb-4 shadow-lg">
                        <div class="card-header" style="background-color: LightPink">
                            <h2 class="">
                                <i class="text-danger">Error500</i> 
                                <small class="text-white text-xs">Internal Server Error</small>
                            </h2>
                        </div>
                        <div class="card-body" style="background-color: LavenderBlush">
                            <div class="text-center font-weight-bold">
                                <h3 class="h2 text-danger mb-2"> 
                                    <i class="fa fa-sad-tear fa-2x"></i><br>
                                    <?php echo $title; ?></h3>

                            </div>
                            <hr>
                            <?php if (__ENVIRONMENT__ == 'development') {
                                ?>

                                <h5>File</h5>
                                <p class="text-xs">
                                    <?php echo $exception->getFile() . ' <b>( Line:' . $exception->getLine() . ')</b>'; ?>
                                </p>
                                <h5>Message</h5>
                                <p>
                                    <?php echo $exception->getMessage(); ?>
                                </p>
                                <h5>Trace</h5>
                                <p>
                                    <?php echo nl2br($exception->getTraceAsString()); ?>
                                </p>
                                <?php if (!empty($query)) { ?>
                                    <h4>Query</h4>
                                    <p><?php echo $query; ?></p>
                                    <h4>Query-binds</h4>
                                    <p>
                                        <?php print_r($binds); ?>
                                    </p>
                                    <?php
                                }
                            } else {
                                ?>
                                    <p>Por favor indique el <b>#Num</b>. Error al administrador del sistema para poder ver la causa de la excepción.</p>
                            <?php } ?>
                        </div>

                        <div class="card-footer">
                            <button class="btn btn-danger btn-block"  onclick='history.back()'>Volver</button>
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </body>
</html>
