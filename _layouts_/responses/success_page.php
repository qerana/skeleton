<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-15" />
        <meta name="lang" content="es" />
        <meta name="author" content="qerana" />
        <meta name="organization" content="qerana" />
        <meta name="locality" content="qworld" />
        <title>:-)<?php echo __APPNAME__; ?></title>
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    </head>
    <body style="background-color: #F8F8F8;" >
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xl-6 col-lg-12 col-md-9">
                    <div class="card mb-4 ">
                        <div class="card-header py-3">
                            <h2 class="">
                                <small class="text-success"><?php echo $title; ?></small>
                            </h2>
                        </div>
                        <div class="card-body py-3" style="background-color: azure">
                            <p>
                                <?php echo $description; ?>
                            </p>
                        </div>
                        <hr>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
<?php
die();
