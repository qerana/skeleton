<?php

/*
 * Copyright (C) 2019 diemarc diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\welcome\controller;

use Qerapp\qaccess\model\user\UserService;

defined('__APPFOLDER__') OR exit('Direct access to this file is forbidden, siya');
/*
  |*****************************************************************************
  | CONTROLLER CLASS  Account
  |*****************************************************************************
  |
  | Controller Account
  | @author qdevtools,
  | @date 2020-10-14 05:49:53,
  |*****************************************************************************
 */

class AccountController extends \Qerana\core\QeranaC {

    public function __construct() {
        parent::__construct();
    }

    /**
     * -------------------------------------------------------------------------
     * View user profile
     * -------------------------------------------------------------------------
     * @param int $id
     * @return void
     */
    public function profile(): void {

        $id = $_SESSION['Q_id_user'];

        $UserService = new UserService;
        $vars = [
            'id_user' => $id,
            'User' => $UserService->getById($id),
            'Plugins' => [
                'data_json.js',
                'app/user.js'
            ]
        ];

        \Qerana\core\View::showView('qaccess/user_profile', $vars);
    }

    /**
     *  show recovery form
     */
    public function recovery() {
        include __APPFOLDER__ . '/welcome/view/qaccess/recovery.php';
        die();
    }

    /**
     *  process recovery tasks
     */
    public function dorecovery() {

        $Config = \Qerana\Configuration::singleton();

        $UserTokenService = new UserTokenService;
        $UserTokenService->createReActivationToken($Config->get('_id_account_activation'));

        \QException\Exceptions::ShowSuccessfulRedirect('ReActivación de cuenta en proceso', 'Sigue las instrucciones que se ha enviado a tu email', '/welcome/login/showLoginPage', 2000);
    }

    /**
     * modify user service
     */
    public function modifyUser() {

        $UserService = new UserService;
        $UserService->save();
    }

}

