<?php

/*
 * Copyright (C) 2019 diemarc diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\welcome\controller;

use Qerapp\qaccess\model\user\UserService;

defined('__APPFOLDER__') OR exit('Direct access to this file is forbidden, siya');
/*
  |*****************************************************************************
  | CONTROLLER CLASS  Profile
  |*****************************************************************************
  |
  | Controller Profile
  | @author qdevtools,
  | @date 2020-10-08 18:30:17,
  |*****************************************************************************
 */

class ProfileController extends \Qerana\core\QeranaC {

    public function __construct() {
        parent::__construct();
    }

    /**
     * -------------------------------------------------------------------------
     * View user profile
     * -------------------------------------------------------------------------
     * @param int $id
     * @return void
     */
    public function profile(): void {

        $id = $_SESSION['Q_id_user'];

        $UserService = new UserService;
        $vars = [
            'id_user' => $id,
            'User' => $UserService->getById($id),
            'Plugins' => [
                'data_json.js',
                'app/user.js'
            ]
        ];

        \Qerana\core\View::showView('qaccess/user_profile', $vars);
    }

    /**
     * -------------------------------------------------------------------------
     * Edit  user profile
     * -------------------------------------------------------------------------
     * @param int $id
     * @return void
     */
    public function editprofile(): void {

        $id = $_SESSION['Q_id_user'];

        $UserService = new UserService;
        $vars = [
            'id_user' => $id,
            'User' => $UserService->getById($id),
            'Plugins' => [
                'data_json.js',
                'app/user.js'
            ]
        ];

        \Qerana\core\View::showForm('qaccess/user_edit', $vars);
    }

    /**
     * -------------------------------------------------------------------------
     * Edit  user profile
     * -------------------------------------------------------------------------
     * @param int $id
     * @return void
     */
    public function changemypassword(): void {

        $id = $_SESSION['Q_id_user'];

        $UserService = new UserService;
        $vars = [
            'id_user' => $id,
            'User' => $UserService->getById($id),
        ];

        \Qerana\core\View::showForm('qaccess/user_password', $vars);
    }

    /**
     *  Modify user data
     */
    public function modify() {

        $UserService = new UserService;
        $UserService->save();
    }

    /**
     *  Cambia la contraseña del usuario actual
     */
    public function dochangemypassword() {

        $UserService = new UserService;
        $UserService->changePassword($_SESSION['Q_id_user']);
        \QException\Exceptions::ShowSuccessful('Qaccess.Profile', 'Password cambiado correctamete');
    }

}

