<?php

/*
 * This file is part of qerana
 * Copyright (C) 2020  tyrion.jackson@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace app\welcome\controller;

use Qerapp\qbasic\model\module\ModuleService,
    Qerapp\qaccess\model\user\UserTokenService,
    Qerapp\qaccess\model\user\UserService;

/**
 * *****************************************************************************
 * Description of WelcomeController
 * *****************************************************************************
 *
 * @author diemarc
 * *****************************************************************************
 */
class WelcomeController extends \Qerana\core\QeranaC {

    protected
            $_Welcome;

    public function __construct() {
        parent::__construct();
    }

     public function index() {


        //check if module qaccess is installed
        $ModuleService = new ModuleService();
        $ModuleService->module_name = 'qaccess';
        $Qaccess = $ModuleService->getModule();
        $tpl_load = ($Qaccess) ? 'index' : 'welcome';

        $content = [
            'Qaccess' => $Qaccess,
        ];

        \Qerana\core\View::showView('welcome', $content);
    }
    /**
     *  process recovery tasks
     */
    public function dorecovery() {

        $Config = \Qerana\Configuration::singleton();

        $UserTokenService = new UserTokenService;
        $UserTokenService->createReActivationToken($Config->get('_id_account_activation'));

        \QException\Exceptions::ShowSuccessfulRedirect('ReActivación de cuenta en proceso', 'Sigue las instrucciones que se ha enviado a tu email', '/welcome/login/showLoginPage', 2000);
    }

    /**
     * -------------------------------------------------------------------------
     * View user profile
     * -------------------------------------------------------------------------
     * @param int $id
     * @return void
     */
    public function profile(): void {

        $id = $_SESSION['Q_id_user'];

        $UserService = new UserService;
        $vars = [
            'id_user' => $id,
            'User' => $UserService->getById($id),
            'Plugins' => [
                'data_json.js',
                'app/user.js'
            ]
        ];

        \Qerana\core\View::showView('qaccess/user_profile', $vars);
    }

    /**
     * -------------------------------------------------------------------------
     * Edit  user profile
     * -------------------------------------------------------------------------
     * @param int $id
     * @return void
     */
    public function editprofile(): void {

        $id = $_SESSION['Q_id_user'];

        $UserService = new UserService;
        $vars = [
            'id_user' => $id,
            'User' => $UserService->getById($id),
            'Plugins' => [
                'data_json.js',
                'app/user.js'
            ]
        ];

        \Qerana\core\View::showForm('qaccess/user_edit', $vars);
    }

    /**
     * -------------------------------------------------------------------------
     * Edit  user profile
     * -------------------------------------------------------------------------
     * @param int $id
     * @return void
     */
    public function changemypassword(): void {

        $id = $_SESSION['Q_id_user'];

        $UserService = new UserService;
        $vars = [
            'id_user' => $id,
            'User' => $UserService->getById($id),
        ];

        \Qerana\core\View::showForm('qaccess/user_password', $vars);
    }

    /**
     *  Cambia la contraseña del usuario actual
     */
    public function dochangemypassword() {

        $UserService = new UserService;
        $UserService->changePassword($_SESSION['Q_id_user']);
        \QException\Exceptions::ShowSuccessful('Qaccess.Profile', 'Password cambiado correctamete');
    }

    /**
     *  Activate user account
     * @param string $token
     */
    public function verification(string $token = '') {

        if (empty($token)) {
            \QException\Exceptions::showHttpStatus(404, 'token verification without token!!');
        }

        $UserTokenService = new UserTokenService;
        $UserTokenService->set_token($token);
        if ($UserTokenService->verifyUserToken()) {
            \helpers\Redirect::to('/welcome/welcome/useractivation');
        }
    }

    /**
     *  load user activation form
     */
    public function useractivation() {
        if (!isset($_SESSION['S_activation_tokenid']) AND!isset($_SESSION['S_token_id_user'])) {

            \QException\Exceptions::showHttpStatus(404, 'trying to access activation user without id_token sessioned!!');
        } else {
            include __APPFOLDER__ . '/welcome/view/qaccess/user_activation.php';
        }
    }

    /**
     *   Activate account
     */
    public function activate() {
        $UserTokenService = new UserTokenService();
        $UserTokenService->activateUser();
        \QException\Exceptions::ShowSuccessfulRedirect('Cuenta activada correctamente', 'Espere mientras preparamos tu pantalla de login', '/welcome/login/showLoginPage');
    }

}
