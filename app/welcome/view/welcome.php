<div class="container">
    <div class="card o-hidden border-0 shadow-lg my-5">
        <div class="card-body p-0">
            <!-- Nested Row within Card Body -->
            <div class="row">
                <div class="col-lg-5 d-none d-lg-block bg-qerana-image">
                    <div class="p-5">
                        <div class="text-center">
                            <h1 class="h4 text-info  mb-4"><?php echo __APPNAME__; ?> !</h1>
                        </div>

                        <hr>

                    </div>
                </div>
                <div class="col-lg-7">
                    <div class="p-5">
                        <div class="text-center">
                            <h5>Default welcome view</h5>
                        </div>

                        <?php if ($Qaccess) { ?>
                            <div class="text-center">
                                <a class="small" href="/welcome/login/showLoginPage"> Login!</a>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>