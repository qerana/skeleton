<?php

/*
 * This file is part of qerana core
 * Copyright (C) 2020  tyrion.jackson@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Config;

defined('__APPFOLDER__') OR exit('Direct access to this file is forbidden, siya');


/*
  |--------------------------------------------------------------------------
  | DevelopmentConfiguration for Qerana
  |--------------------------------------------------------------------------
  |
 */


//url position for each url-component
$this->config->set('position_module', 0);
$this->config->set('position_controller', 1);
$this->config->set('position_action', 2);

// default layout 
$this->config->set('default_layout','default');

// default routes

$this->config->set('default_module','welcome');
$this->config->set('default_controller','welcome');
$this->config->set('default_action','index');


// sufix mvc

$this->config->set('controller_sufix','Controller');
$this->config->set('model_sufix','Model');


/*
  |--------------------------------------------------------------------------
  | SECURITY
  |--------------------------------------------------------------------------
  |
 */

/** determine if ACL module will be run in each http-petition */
$this->config->set('_acl_active_', true);


/**  determine with level of ACL must be check 
1: profile-module,2:profile-module-controller; 3:profile-module-controller-action
 *  */
$this->config->set('_acl_level_', '1');


/** determine user creation activation (token,direct)*/
$this->config->set('_activation_user_', 'direct');

/**
 *  namespaces with private modules that can pass acl control 
 */
$this->config->set('_access_exceptions_',
        [
            '\app\welcome\controller\WelcomeController',
            '\app\welcome\controller\AccountController',
            '\app\welcome\controller\LoginController',
            '\Qerapp\qcontent\controller\PageController'
            ]);
/*
 * ---------------------------------------------------------------
 * AES Key
 * ---------------------------------------------------------------
 */

$this->config->set('_aeskey_', 'vnaT497*_N*98/(1#jkQ9X&2');

/**
 * -----------------------------------------------------------------------------
 * Sessions
 * -----------------------------------------------------------------------------
 */
/** @var mixed , session name */
$this->config->set('_session_name_', '_qfwsess_');

/** @var boolean, if overt https set to true */
$this->config->set('_session_https_', true);

/** @var boolean, only url no js mode, */
$this->config->set('_session_http_only_', true);

/** @var mixed, algoritmo hash para encriptar las sesiones
 *  usar (hash_algos() para ver los disponibles) */
$this->config->set('_session_hash_', 'sha512');

/**
 * -----------------------------------------------------------------------------
 * MIX
 * -----------------------------------------------------------------------------
 */

$this->config->set('_id_account_activation','1');
$this->config->set('_id_account_recovery','1');
