 $(document).on('keyup keypress', 'form input[type="text"]', function (e) {
        if (e.keyCode === 13) {
           $('#df-form').submit();
           return false;
        }
    });

$(document).ready(function () {


    // populate order directions 
    $(".df-order").each(function () {
        $(this).append(' <i class="df-order-direction fa fa-angle-down" id="order-desc" title="Descendente" data-direction="DESC" style="cursor:pointer"></i> ');
        $(this).prepend(' <i class="df-order-direction fa fa-angle-up" id="order-asc" title="Ascendente" data-direction="ASC" style="cursor:pointer"></i> ');
    });


    filterData();

    // set orderby 
    order_direction = '';
    $('.df-order').click(function () {
        order_by = $(this).attr("data-order");
        // set order direction
        $('.df-order-direction').click(function () {
            
            $('.df-order-direction').removeClass('text-white');
            order_direction = $(this).attr("data-direction");
            $(this).addClass('text-white');
        });
        // run
        filterData();
    });

    // serialize all inputs filled
    $('#df-form').on('submit', function (e) {
        e.preventDefault();
        // data_serialized = $('#df-form').serialize();
        data_serialized = $('#df-form :input').filter(function (i) {
            return ($(this).val().length !== 0);
        }).serialize();
        // run
        filterData();
    });


    // send on-change event tags
    $('.df-change').on('change', function (e) {
        //e.preventDefault();
        data_serialized = $('#df-form :input').filter(function (i) {
            return ($(this).val().length !== 0);
        }).serialize();
        // run
        filterData();
    });


    // clean form 
    $('.df-clean').click(function () {
        $('input[name="df_reset"]').remove();

        $('#df-form')[0].reset();
        $('<input>').attr({
            type: 'hidden',
            name: 'df_reset',
            value: '1'

        }).appendTo('form');


        // $('#df-form')[0].reset();

        data_serialized = $('#df-form :input[name="df_reset"]').serialize();
        $("select option[value='']").attr('selected', 'selected');
        // run
        filterData();
        

    });


});

/**
 * -----------------------------------------------------------------------------
 * run the data filter
 * -----------------------------------------------------------------------------
 * @param {type} ofset
 * @param {type} url_filter
 * @param {type} callbackFunction
 * @returns {undefined}
 */
function runDataFilter(ofset, url_filter, callbackFunction) {

    // draw a loader
    drawLoader();

    // draw a pager
    drawPager();

    // if ofset is undefined then equal to 0
    var ofset_filter = (typeof ofset === 'undefined') ? '' : '/' + ofset;

    // show loader
    $('#loader_filter').removeClass('d-none');
    $('.df-data').hide();

    // order_param

    var order_param = (typeof order_by !== 'undefined') ? '&df-order-by=' + order_by : '';
    var order_dir = (typeof order_direction !== 'undefined') ? '&df-order-direction=' + order_direction : '';
    var param_to_search = (typeof data_serialized !== 'undefined') ? data_serialized : '';


    // url to server script 
    url_to_run = url_filter + ofset_filter + '/?' + param_to_search + order_param + order_dir;

    // get json data
    $.getJSON(url_to_run, function (data) {

        // hide the loader
        $('#loader_filter').addClass('d-none');
        $('.df-data').show();

        // build buttons prev,next
        drawPagerButtons(data.record.ofset, data.record.next, data.record.prev, data.record.total);

        // total record
        $('#total').html(data.record.total);

        // string record
        var reg_show = (data.record.next > data.record.total) ? data.record.total : data.record.next;

        // fill params fields

        setParamsFilter(data.record.params);


        $('#desc').append(reg_show + ' / ' + data.record.total + ' ');

        // assign to data_filtered var, to procees in callbackfucntion
        data_filtered = data.record.Result;

        // run the callback function
        var func = new Function(callbackFunction);
        func();


    });

}

/**
 * -----------------------------------------------------------------------------
 * Set the params sended to filter to iu
 * -----------------------------------------------------------------------------
 * @param {type} params
 * @returns {undefined}
 */
function setParamsFilter(params) {

    $.each(params, function (param_name, param_value) {

        if (param_name !== '') {
            type_element = $('[name="' + param_name + '"]').get(0).tagName;

            if (type_element === 'INPUT') {
                $('input[name="' + param_name + '"]').val(param_value);
            }

            if (type_element === 'SELECT') {
                $('select[name="' + param_name + '"] option[value=' + param_value + ']').attr('selected', 'selected');
            }
        }



    });


}


///**
// * -----------------------------------------------------------------------------
// * reset form
// * -----------------------------------------------------------------------------
// * @returns {undefined}
// */
//function resetForm() {
//
//
//    $('#df-form')[0].reset();
//    $('<input>').attr({
//        type: 'd-none',
//        name: 'df_reset',
//        value: '1'
//
//    }).appendTo('form');
//
//
//    $('#df-form')[0].reset();
//
//    data_serialized = $('#df-form :input[name="df_reset"]').serialize();
//    // run
//    filterData();
//
//}


/**
 * -----------------------------------------------------------------------------
 * Draw pager buttons
 * -----------------------------------------------------------------------------
 * @param {type} ofset
 * @param {type} val_next
 * @param {type} val_prev
 * @param {type} total_rows
 * @returns {undefined}
 */
function drawPagerButtons(ofset, val_next, val_prev, total_rows) {

    var html_prev = '<button type="button" class="btn btn-sm btn-primary" ';
    html_prev += ' onclick="filterData(' + val_prev + ')" title="Anterior">';
    html_prev += '<i class="fa fa-arrow-left"></i>';
    html_prev += '</button> ';

    var html_next = '<button type="button" class="btn btn-sm btn-primary" ';
    html_next += 'onclick="filterData(' + val_next + ')" title="Siguiente">';
    html_next += '<i class="fa fa-arrow-right"></i>';
    html_next += '</button> ';

    var button_prev = (ofset > 0) ? html_prev : '';
    var button_next = (val_next < total_rows) ? html_next : '';

    $('#prev').html(button_prev);
    $('#next').html(button_next);


}

/**
 * -----------------------------------------------------------------------------
 * draw a basic loader
 * -----------------------------------------------------------------------------
 * @returns {undefined}
 */
function drawLoader() {

    var html_loader = '<div class="d-none text-default" align="center" id="loader_filter">';
    html_loader += '<i class="fa fa-spinner fa-spin fa-4x"></i> <br>Filtrando...';
    html_loader += '</div>';

    $('#df-loader').html(html_loader);

}
/**
 * -----------------------------------------------------------------------------
 * draw a basic pager
 * -----------------------------------------------------------------------------
 * @returns {undefined}
 */
function drawPager() {

    var html_pager = '<ul class="pagination pagination-sm pull-right no-margin bg-gray" style="padding:5px">';
    html_pager += '<li id="desc" class="text-blue"></li>';
    html_pager += '<li id="prev"></li>';
    html_pager += '<li id="next"></li>';
    html_pager += '</ul>';

    $('#df-pager').html(html_pager);

}
