
/**
 * open a link, contained in data-href element tag
 * @param {type} object
 * @returns {undefined}
 */
function openHref(object) {
    window.location = $(object).data("href");
}
    
